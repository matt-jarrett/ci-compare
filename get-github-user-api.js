const fetch = require("node-fetch")

module.exports.getGitHubUser = async (userName) => {
  const url = `https://api.github.com/users/${userName}`
  const response = await fetch(url)
  const user = await response.json()
  return user
}

const test = require("tape-async")
const { getGitHubUser } = require("./get-github-user-api.js")

test("Integration - GitHub", async (assert) => {
  assert.plan(2)
  const { name, location } = await getGitHubUser("cujarrett")
  console.log(name, location)
  assert.equal(name, "Matt Jarrett", "GitHub username verified")
  assert.equal(location, "Dallas, Texas", "GitHub user location verified")
})

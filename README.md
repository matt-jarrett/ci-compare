## Work In Progress

![logo](./media/logo.jpg)

## What's it do?
A comparison of popular cloud based CI tools measured by running a small consistent set of tasks.
It compares container based solutions versus a VM based solution. This information can get outdated
very quickly as services evolve. This evaluation was done on 2019-03-10 and the CI Tools are likely
updated since.

## What is Continuous Integration (CI)?
Continuous Integration (CI) is a development practice that requires developers to integrate code
into a shared repository several times a day. Each check-in is then verified by an automated build,
allowing teams to detect problems early.
- By integrating regularly, you can detect errors quickly, and locate them more easily.
- Stop waiting to find out if your code’s going to work
- Increase visibility enabling greater communication
- Avoid Integration hell

## Things to look for in a CI Tool
- Container based solution - More flexible and scalable, less provisioning headaches
- Performance
- Ease of job orchestration
- Notifications
- Caching Possibilities
- Metrics & Insights
- Configuration as code

### Container Based CI tools observed
- [GitHub Actions](https://github.com/cujarrett/ci-compare/actions)
- [GitLab CI](https://gitlab.com/matt-jarrett/ci-compare/pipelines)
- [Circle CI](https://circleci.com/gh/cujarrett/workflows/ci-compare/tree/master)

### VM Based CI tool observed
- [Travis CI](https://travis-ci.org/cujarrett/ci-compare)

## Task for each pipeline
A mix of linear and parallel tasks

- Build (install node dependencies)
- Test
  - Read file via grep
  - Security Audit dependencies for vulnerabilities
  - Lint project
  - Hit REST API via Node package
- (Mock) Deploy

![diagram of the pipeline](./media/ci-diagram.jpg)

## Evaluation

### GitHub Actions (Container Based CI)
[![Build Status](https://action-badges.now.sh/cujarrett/ci-compare)](https://github.com/cujarrett/ci-compare/actions)

Shortest Pipeline Duration: `49 seconds`

Longest Pipeline Duration: `1 minute and 9 seconds`

![github-actions-demo](./media/github-actions-demo.gif)

- UI editor for building pipelines is really good. Addictively enjoyable
  ![create-github-actions-demo](./media/create-github-actions.gif)

- :+1: Fast
- :+1:Running pipeline is very easy understand what job each job is doing
- :+1: Cloud native - integrations with AWS, Heroku, Google Cloud, and others
- :ok_hand: Still in public beta
- :-1: No native bash support but can be done with setting up a Dockerfile for these needs
- :-1: Missing basics like pipeline badge
- :-1: Closed source - hard to know feature roadmap

### GitLab CI (Container Based CI)
[![pipeline status](https://gitlab.com/matt-jarrett/ci-compare/badges/master/pipeline.svg)](https://gitlab.com/matt-jarrett/ci-compare/commits/master)

Shortest Pipeline Duration: `2 minutes 51 seconds`

Longest Pipeline Duration: `3 minutes 10 seconds`

![gitlab-ci-demo](./media/gitlab-ci.gif)

- Baked into GitLab very well - GitLab is a single application for the entire DevOps lifecycle
- CI/CD config linter is great for finding mistakes in your CI/CD config while creating or maintaining
- :+1: Good docs
- :+1: Built in secure Container Registry makes namespace issues a non-concern
- :+1: API is very nice
- :+1: Review Apps built in - Review dynamic live environments for each development branch
- :+1: Cloud native - integrations with AWS, Heroku, Google Cloud, and others
- :+1: Integrated debugging with web integrated web terminal
- :+1: Offers scheduling of pipelines
- :+1: Manual deployment, and effortless rollback capabilities
- :-1: Doesn't play well with GitHub
- :-1: GitLab secrets are not that secret - I prefer the non viewable GitHub Actions secrets

### Circle CI (Container Based CI)
[![CircleCI](https://circleci.com/gh/cujarrett/ci-compare/tree/master.svg?style=svg)](https://circleci.com/gh/cujarrett/ci-compare/tree/master)

Shortest Pipeline Duration: `17 seconds`

Longest Pipeline Duration: `44 seconds`

![circle-ci-demo](./media/circle-ci.gif)

- Fast
- :+1: Good docs with plenty of examples
- :+1: Healthy image support and documentation
- :+1: API is very nice
- :+1: Cloud native - integrations with AWS, Heroku, Google Cloud, and others
- :-1: Weird that Circle CI pushes their own images so much they use a Node.js image for a Hello World bash example.

### Travis CI (VM Based CI)
[![Build Status](https://travis-ci.org/cujarrett/ci-compare.svg?branch=master)](https://travis-ci.org/cujarrett/ci-compare)

![github-actions-demo](./media/travis-ci.gif)

Shortest Pipeline Duration: `3 minutes 43 seconds`

Longest Pipeline Duration: `6 minutes 42 seconds`

- :+1: Good language support including macOS
- :+1: Good docs
- :-1: No `.travis.yml` lint tool for finding mistakes when building CI config makes debugging config syntax problems a headache
- :-1: Missing comprehensive pipeline graphs - It's hard to know what job is what in each stage while it's running
- :-1: Slowest of the options compared becuase of it not being a container based solution

Made with :heart: and JavaScript.

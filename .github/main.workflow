workflow "CI/CD Pipeline" {
  on = "push"
  resolves = ["Security Audit", "Lint", "Test", "Mock Deployment"]
}

action "Build" {
  uses = "actions/npm@59b64a598378f31e49cb76f27d6f3312b582f680"
  args = "install"
}

action "Read File" {
  uses = "./github-action-bash-container"
  needs = ["Build"]
  args = "grep -c Dog ./test-file.txt"
}

action "Security Audit" {
  uses = "actions/npm@59b64a598378f31e49cb76f27d6f3312b582f680"
  needs = ["Build"]
  args = "audit"
}

action "Lint" {
  uses = "actions/npm@59b64a598378f31e49cb76f27d6f3312b582f680"
  needs = ["Build"]
  args = "run lint"
}

action "Test" {
  uses = "actions/npm@59b64a598378f31e49cb76f27d6f3312b582f680"
  needs = ["Build"]
  args = "run test"
}

action "Mock Deployment" {
  uses = "./github-action-bash-container"
  needs = ["Read File", "Security Audit", "Lint", "Test"]
  args = "echo 'Mock deployment to public cloud'"
}

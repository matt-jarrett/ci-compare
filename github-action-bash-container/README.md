#### What this GitHub Action does
It exposes a bash container for pipeline needs.

#### Environment
Linux `debian:9.5-slim`

#### Environment variables the action uses
None

#### Secrets the action uses
None

#### Required arguments
None

#### Optional arguments
Whatever you wish the bash to execute. No quotes needed.
